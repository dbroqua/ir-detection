import os
import RPi.GPIO as GPIO
import time
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

fromaddr = os.environ.get('fromaddr') 
toaddr = os.environ.get('toaddr')
msg = MIMEMultipart()
msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = "Intrusion in deskroom!"

server = smtplib.SMTP(os.environ.get('mailserver'), os.environ.get('mailport'))
server.starttls()
server.login(os.environ.get('mailuser'), os.environ.get('mailpwd'))

def my_callback( test ):
        body = 'New user detected in deskroom'
        msg.attach(MIMEText(body, 'plain'))
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        print 'Intrusion !'

GPIO.add_event_detect(4, GPIO.RISING, callback=my_callback, bouncetime=300)

try:
    while True:
        time.sleep(1)

except KeyboardInterrupt:
    print "Quit"
    GPIO.cleanup()

